-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: mutant_homolog
-- ------------------------------------------------------
-- Server version	5.5.5-10.0.34-MariaDB-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,'Mutant Website','bearned',NULL,'http://mutantbr.com','*','*',NULL,'fa fa-window-maximize',NULL,9,1,1,1,'','2017-11-10 13:31:26','2018-04-17 22:26:23',NULL,1,'_blank',''),(4,'BadgeTracker','badgetracker','BadgeTracker',NULL,'*','*',NULL,NULL,NULL,NULL,0,1,0,NULL,'2017-11-30 17:00:01','2017-11-30 17:29:09',NULL,NULL,NULL,''),(5,'Card Feed','cardfeed','CardFeed',NULL,'*','*',NULL,'fa fa-rss',NULL,NULL,0,1,0,NULL,'2017-11-30 17:00:01','2017-11-30 17:00:01',NULL,NULL,NULL,''),(6,'Grupos','groupon','GroupOn','/groups','*','*',NULL,'fa fa-users',NULL,1,1,1,0,'','2017-11-30 17:00:01','2018-04-17 22:15:01',NULL,NULL,NULL,''),(7,'JQuest','jquest','JQuest',NULL,'*','*',NULL,NULL,NULL,NULL,0,1,0,NULL,'2017-11-30 17:00:01','2017-11-30 17:00:01',NULL,NULL,NULL,''),(8,'Relacionamento','ohchat','OhChat','/chat','*','*',NULL,'fa fa-comments-o',NULL,2,1,0,0,NULL,'2017-11-30 17:00:01','2018-04-17 22:26:23',NULL,NULL,NULL,''),(21,'Enquete e Pesquisa','enquete','Jquest','/enquetes','*','*',NULL,'fa fa-question-circle',24,10,1,1,0,'Grupo Teste','2017-11-30 19:20:35','2018-04-17 22:26:23',NULL,NULL,NULL,''),(24,'Administrador','administrador',NULL,'','system','admin',NULL,'fa fa-cog',NULL,14,1,0,0,'Grupo Administrador','2017-11-30 20:56:17','2018-04-17 22:26:23',1,NULL,NULL,''),(36,'R&S','rs',NULL,'/talentos/rs','*','*',NULL,'fa fa-arrow-right',41,16,1,1,0,'Grupo Talentos e Desenvolvimento','2018-04-17 22:11:40','2018-04-17 22:26:23',1,NULL,NULL,''),(37,'Carreira','carreira',NULL,'/talentos/carreira','*','*',NULL,'fa fa-arrow-right',41,4,1,1,0,'Grupo Comunicação e Engajamento','2018-04-17 22:13:08','2018-04-17 22:26:23',1,NULL,NULL,''),(38,'Integração','integracao',NULL,'/talentos/integracao','*','*',NULL,'fa fa-arrow-right',41,1,1,1,0,'Grupo Folha e Benefícios','2018-04-17 22:13:27','2018-04-17 22:19:44',1,NULL,NULL,''),(39,'Treinamento','treinamento',NULL,'/talentos/treinamento','*','*',NULL,'fa fa-arrow-right',41,5,1,1,0,'Grupo Saúde e Segurança','2018-04-17 22:13:36','2018-04-17 22:26:23',1,NULL,NULL,''),(41,'Talentos e Desenvolvimento','talentose_desenvolvimento',NULL,'','*','*',NULL,'fa fa-male',NULL,2,1,1,0,'Grupo Talentos e Desenvolvimento','2018-04-17 22:18:21','2018-04-17 22:26:23',1,NULL,NULL,''),(42,'Comunicação e Engajamento','comunicacaoe_engajamento',NULL,'','*','*',NULL,'fa fa-smile-o',NULL,3,1,1,0,'Grupo Talentos e Desenvolvimento','2018-04-17 22:18:21','2018-04-17 22:26:23',1,NULL,NULL,''),(43,'Clima','clima',NULL,'/comunicacao-e-engajamento/clima','*','*',NULL,'fa fa-arrow-right',42,6,1,1,0,'Grupo Saúde e Segurança','2018-04-17 22:13:36','2018-04-17 22:26:23',1,NULL,NULL,''),(44,'Folha e Benefícios','folhae_beneficios',NULL,'/folha-e-beneficios','*','*',NULL,'fa fa-gift',NULL,8,1,1,0,'Grupo Saúde e Segurança','2018-04-17 22:13:36','2018-04-17 22:26:23',1,NULL,NULL,''),(45,'Saúde e Segurança','saudee_seguranca',NULL,'/bem-estar','*','*',NULL,'fa fa-heart',NULL,7,1,1,0,'Grupo Saúde e Segurança','2018-04-17 22:13:36','2018-04-17 22:26:11',1,NULL,NULL,'');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-18 12:04:56